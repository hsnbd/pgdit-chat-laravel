<?php

namespace App\Http\Controllers;

use App\Batch;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class AuthorityController extends Controller
{
    const IMAGE_PATH = 'public/students/';
    const VIEW_PATH = 'authority.authorities.';

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $authorities = User::where('role', '!=', User::ROLE_STUDENT)->get();

        return view(self::VIEW_PATH.'index', compact('authorities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $batches = Batch::where('status', 1)->latest()->get();

        $authority = new User();

        $this->authorize('viewAny', $authority);

        $roles = [
          User::ROLE_CR => 'CR',
          User::ROLE_TEACHER => 'Teacher',
          User::ROLE_STAFF => 'Staff',
        ];

        return view(self::VIEW_PATH.'edit', compact('batches', 'authority', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $this->validator($input)->validate();

        try {
            DB::beginTransaction();
            $input['image'] = md5(microtime()).'.'.$request->file('image')->extension();
            $input['password'] = Hash::make(strlen($input['password']) ? $input['password'] : '123456');

            $authority = User::create($input);

            Storage::putFileAs(
                self::IMAGE_PATH,
                $request->file('image'),
                $authority->image
            );
            DB::commit();

        } catch (\Throwable $exception) {
            Log::debug($exception->getMessage());
            DB::rollBack();
            return back()
                ->with(["error" => "Something Wrong. Please Try Again"])
                ->withInput();
        }

        return back()->with(["success" => "Authority Successfully Created"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Factory|View
     * @throws \Exception
     */
    public function show($id)
    {
        $authority = User::findOrFail($id);

        $this->authorize('viewAny', $authority);

        return view(self::VIEW_PATH.'view', compact('authority'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|View
     * @throws \Exception
     */
    public function edit($id)
    {
        $authority = User::findOrFail($id);

        $this->authorize('viewAny', $authority);

        $batches = Batch::where('status', 1)->latest()->get();

        $roles = [
            User::ROLE_CR => 'CR',
            User::ROLE_TEACHER => 'Teacher',
            User::ROLE_STAFF => 'Staff',
        ];

        return view(self::VIEW_PATH.'edit', compact('authority', 'batches', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $authority = User::findOrFail($id);

        $input = $request->except('_token');

        $this->validator($input, $id)->validate();

        try {
            DB::beginTransaction();

            if (strlen($request->post('password'))) {
                $input['password'] = Hash::make($input['password']);
            } else {
                unset($input['password']);
            }

            if ($request->hasFile('image')) {
                $input['image'] = md5(microtime()).'.'.$request->file('image')->extension();

                if ($authority->image && Storage::exists(self::IMAGE_PATH.$authority->image)) {
                    Storage::delete(self::IMAGE_PATH.$authority->image);
                }

                Storage::putFileAs(
                    self::IMAGE_PATH,
                    $request->file('image'),
                    $input['image']
                );
            }
            if ($request->has('image_status') && $request->post('image_status') < 1) {
                $input['image'] = null;
                Storage::delete(self::IMAGE_PATH.$authority->image);
            }

            $authority->update($input);

            DB::commit();
        } catch (\Throwable $exception) {
            Log::debug($exception->getMessage());
            DB::rollBack();
            return back()
                ->with(["error" => "Something Wrong. Please Try Again"])
                ->withInput();
        }

        return back()->with(["success" => "Authority Successfully Updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $authority = User::findOrFail($id);

        $this->authorize('viewAny', $authority);

        try {
            if ($authority->image && Storage::exists(self::IMAGE_PATH.$authority->image)) {
                Storage::delete(self::IMAGE_PATH.$authority->image);
            }
            $authority->delete();
        } catch (\Throwable $exception) {
            return redirect()->route('admin.authorities.index')
                ->with(["error" => "Something Wrong. Please Try Again"]);
        }
        return redirect()->route('admin.authorities.index')
            ->with(["success" => "Authority Successfully Created"]);
    }

    public function validator(array $data, $id = null)
    {
        $rule = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            "gender" => "required",
            "role" => "required"
        ];
        return Validator::make($data, $rule, []);
    }
}
