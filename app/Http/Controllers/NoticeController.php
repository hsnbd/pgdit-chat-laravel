<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Notice;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class NoticeController extends Controller
{
    const VIEW_PATH = 'authority.notices.';
    const FILE_PATH = 'public/notices/';

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $notices = Notice::latest()->get();

        return view(self::VIEW_PATH. 'index', compact('notices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $batches = Batch::where('status', 1)->latest()->get();

        $notice = new Notice();

        return view(self::VIEW_PATH .'edit', compact('batches','notice'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $this->validator($input)->validate();


        try {
            DB::beginTransaction();

            $input['attachment'] = md5(microtime()) . '.' . $request->file('attachment')->extension();

            $notice = Notice::create($input);

            Storage::putFileAs(
                self::FILE_PATH,
                $request->file('attachment'),
                $notice->attachment
            );
            DB::commit();
        } catch (\Throwable $exception) {
            Log::debug($exception->getMessage());
            DB::rollBack();
            return back()
                ->with(["error" => "Something Wrong. Please Try Again"])
                ->withInput();
        }

        return back()->with(["success" => "Notice Successfully Created"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Notice  $notice
     * @return Factory|View
     */
    public function show(Notice $notice)
    {
        return view(self::VIEW_PATH .'view', compact('notice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Notice  $notice
     * @return Factory|View
     */
    public function edit(Notice $notice)
    {
        $batches = Batch::where('status', 1)->latest()->get();

        return view(self::VIEW_PATH .'edit', compact('notice', 'batches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Notice  $notice
     * @return RedirectResponse
     */
    public function update(Request $request, Notice $notice)
    {
        $input = $request->except('_token');

        $this->validator($input, true)->validate();

        try {
            DB::beginTransaction();

            if ($request->hasFile('attachment')) {

                $input['attachment'] = md5(microtime()) . '.' . $request->file('attachment')->extension();

                if($notice->attachment && Storage::exists(self::FILE_PATH . $notice->attachment)) {
                    Storage::delete(self::FILE_PATH . $notice->attachment);
                }

                Storage::putFileAs(
                    self::FILE_PATH,
                    $request->file('attachment'),
                    $input['attachment']
                );
            }

            $notice->update($input);

            DB::commit();
        } catch (\Throwable $exception) {
            Log::debug($exception->getMessage());
            DB::rollBack();
            return back()
                ->with(["error" => "Something Wrong. Please Try Again"])
                ->withInput();
        }

        return back()->with(["success" => "Notice Successfully Updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Notice  $notice
     * @return RedirectResponse
     */
    public function destroy(Notice $notice)
    {
        try {
            if($notice->attachment && Storage::exists(self::FILE_PATH . $notice->attachment)) {
                Storage::delete(self::FILE_PATH . $notice->attachment);
            }
            $notice->delete();
        } catch (\Throwable $exception) {
            return redirect()->route('admin.notices.index')
                ->with(["error" => "Something Wrong. Please Try Again"]);
        }
        return redirect()->route('admin.notices.index')
            ->with(["success" => "Notice Successfully Deleted"]);
    }

    public function validator(array $data, $edit=false)
    {
        $rule = [
            'title' => ['required', 'string', 'max:255'],
            'attachment' => 'required|mimes:pdf',
            "type" => "required",
            "batch_id" => "required"
        ];
        if ($edit) {
            unset($rule['attachment']);
        }
        return Validator::make($data, $rule, []);
    }
}
