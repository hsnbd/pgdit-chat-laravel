<?php

namespace App\Http\Controllers;

use App\Batch;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $authUser = Auth::user();

        $students = User::whereIn('role', [User::ROLE_STUDENT, User::ROLE_CR]);

        if($authUser->hasRole(User::ROLE_CR)) {
            $totalStudent = $students->where('batch_id', $authUser->batch_id)->count();
            $activeStudent = $students->where('batch_id', $authUser->batch_id)
                ->whereHas('batch', function ($query){
                return $query->where('status', 'active');
            })->count();
        } else {
            $totalStudent = $students->count();
            $activeStudent = $students->whereHas('batch', function ($query){
                return $query->where('status', 'active');
            })->count();
        }


        $totalBatch = Batch::count();
        $activeBatch = Batch::where('status', 'active')->count();

        return view('home', compact('totalStudent', 'activeStudent', 'totalBatch', 'activeBatch'));
    }
}
