<?php

namespace App\Http\Controllers;

use App\Batch;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class StudentController extends Controller
{
    const IMAGE_PATH = 'public/students/';
    const VIEW_PATH = 'authority.students.';

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $authUser = Auth::user();

        $students = User::students();

        if($authUser->hasRole(User::ROLE_CR)) {
            $students->where('batch_id', $authUser->batch_id);
        }
        $students = $students->get();

        return view(self::VIEW_PATH.'index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $batches = Batch::where('status', 1)->latest()->get();

        $student = new User();

        $roles = [
            User::ROLE_CR => 'CR',
            User::ROLE_STUDENT => 'Student',
        ];

        return view(self::VIEW_PATH.'edit', compact('batches', 'student', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $this->validator($input)->validate();

        try {
            DB::beginTransaction();
            $input['image'] = md5(microtime()).'.'.$request->file('image')->extension();
            $input['password'] = Hash::make(strlen($input['password']) ? $input['password'] : '123456');

            $student = User::create($input);

            Storage::putFileAs(
                self::IMAGE_PATH,
                $request->file('image'),
                $student->image
            );
            DB::commit();

        } catch (\Throwable $exception) {
            Log::debug($exception->getMessage());
            DB::rollBack();
            return back()
                ->with(["error" => "Something Wrong. Please Try Again"])
                ->withInput();
        }

        return back()->with(["success" => "Student Successfully Created"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Factory|View
     * @throws \Exception
     */
    public function show($id)
    {
        $student = User::findOrFail($id);

        return view(self::VIEW_PATH.'view', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|View
     * @throws \Exception
     */
    public function edit($id)
    {
        $student = User::findOrFail($id);

        $batches = Batch::where('status', 1)->latest()->get();

        $roles = [
            User::ROLE_CR => 'CR',
            User::ROLE_STUDENT => 'Student',
        ];

        return view(self::VIEW_PATH.'edit', compact('student', 'batches', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $student = User::findOrFail($id);

        $input = $request->except('_token');

        $this->validator($input, $id)->validate();

        try {
            DB::beginTransaction();

            if ($request->hasFile('image')) {
                $input['image'] = md5(microtime()).'.'.$request->file('image')->extension();

                if ($student->image && Storage::exists(self::IMAGE_PATH.$student->image)) {
                    Storage::delete(self::IMAGE_PATH.$student->image);
                }

                Storage::putFileAs(
                    self::IMAGE_PATH,
                    $request->file('image'),
                    $input['image']
                );
            }
            if ($request->has('image_status') && $request->post('image_status') < 1) {
                $input['image'] = null;
                Storage::delete(self::IMAGE_PATH.$student->image);
            }

            $student->update($input);

            DB::commit();
        } catch (\Throwable $exception) {
            Log::debug($exception->getMessage());
            DB::rollBack();
            return back()
                ->with(["error" => "Something Wrong. Please Try Again"])
                ->withInput();
        }

        return back()->with(["success" => "Student Successfully Updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $student = User::findOrFail($id);

        try {
            if ($student->image && Storage::exists(self::IMAGE_PATH.$student->image)) {
                Storage::delete(self::IMAGE_PATH.$student->image);
            }
            $student->delete();
        } catch (\Throwable $exception) {
            return redirect()->route('admin.students.index')
                ->with(["error" => "Something Wrong. Please Try Again"]);
        }
        return redirect()->route('admin.students.index')
            ->with(["success" => "Student Successfully Created"]);
    }

    public function validator(array $data, $id = null)
    {
        $rule = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            "roll" => "required",
            "gender" => "required"
        ];
        return Validator::make($data, $rule, []);
    }
}
