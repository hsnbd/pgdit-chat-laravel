<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UserManagementController extends Controller {
    public function index(Request $request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->save();

        return $user;
    }
}
