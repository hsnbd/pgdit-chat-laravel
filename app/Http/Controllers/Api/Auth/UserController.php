<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function update(Request $request)
    {
        $user = Auth::user();
        $user = User::findOrFail($user->id);

        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|unique:users,email,'.$user->id,
                'password' => 'sometimes|min:6',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->first()], 403);
        }

        try {
            $data['email'] = $request->email;

            if ($request->has('password')) {
                $data['password'] = Hash::make($request->password);
            }
            $user->update($data);

        } catch (\Throwable $exception) {
            return response()->json(["errors" => $exception->getMessage()], 403);
        }

        return response()->json(["message" => "Successfully updated"], 200);
    }
}
