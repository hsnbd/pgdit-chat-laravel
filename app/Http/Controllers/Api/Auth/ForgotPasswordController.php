<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
     */
    use SendsPasswordResetEmails;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getResetToken(Request $request)
    {
        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            return response()->json(null, 400);
        }
        $token = $this->broker()->createToken($user);

        // NOTE: Please Send Token to Email

        $mail = \Mail::send('auth.api.reset_link', array(
            'link' => \URL::route('password.reset', $token),
            'name' => $user->name,
            'text' => "Reset Password"
            ),
            function($message) use ($user) {
             $message->to($user->email, $user->name)->subject('Reset Password');
          });

        return response()->json("Reset link sent to email", 200);
    }
}
