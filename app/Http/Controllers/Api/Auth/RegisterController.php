<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Carbon\Carbon;

class RegisterController extends Controller
{
    use IssueToken;
    private $client;
    public function __construct()
    {
      $this->client = \Laravel\Passport\Client::find(1);
    }
    /*
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
     public function register(Request $request){

        $validator = Validator::make($request->all(), [
          // 'name' => 'required',
       		'email' => 'required|email|unique:users,email',
       		'ip' => 'unique:users,ip',
       		'mac' => 'unique:users,mac',
       		// 'password' => 'required|min:6',
          // 'phone' => 'nullable',
          // 'country' => 'nullable',
        ],
        [
          'ip.unique' => 'This User Already Registered Via Same Device',
          'mac.unique' => 'This User Already Registered Via Same Device',
        ]
      );

        if ($validator->fails()) {
          return response()->json(['errors'=>$validator->errors()]);
        }

       	$user = User::create([
       		'name' => request('name'),
       		'email' => request('email'),
       		'phone' => request('phone'),
       		'ip' => request('ip'),
       		'mac' => request('mac'),
       		'country' => request('country'),
       		'password' => bcrypt(request('password')),
       		'validity_date' =>  Carbon::now()->addDays(3),
       		'subscription_date' =>  Carbon::now(),
          'subscription_type' =>  User::SUBSC_FREE,
       	]);

       	return $this->issueToken($request, 'password');
     }
}
