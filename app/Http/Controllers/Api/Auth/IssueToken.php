<?php

namespace App\Http\Controllers\API\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

trait IssueToken
{
    public function issueToken(Request $request, $grantType, $scope = "")
    {
        $params = [
            'grant_type' => $grantType,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $request->email,
            'password' => $request->password,
            'scope' => $scope
        ];

        $email = $request->email;

        $request->request->add($params);
        $proxy = Request::create('oauth/token', 'POST');

        // return Route::dispatch($proxy);
        $tokenResponse = Route::dispatch($proxy);

        $content = $tokenResponse->getContent();

        //convert json to array
        $data = json_decode($content, true);

        if (isset($data["error"])) {
            return response()->json(["errors" => "The user credentials were incorrect."], 403);
        }

//         $user = User::where('email', '=', $email)->first();
//        // $user = array_remove_null($user);
//        //add access token to user
//        // $user = collect($user)->except('created_at', 'updated_at');
        $user = User::where('email', '=', $request->email)->first();
        if ($user->batch->status != 'active') {
            return response()->json(["errors" => "Access Forbidden"], 403);
        }

        if ($user->role === User::ROLE_TEACHER || $user->role === User::ROLE_STAFF) {
            return response()->json(["errors" => "Only Allowed For Student."], 403);
        }

        $user = collect([]);

        $user->put('token_type', $data['token_type']);
        $user->put('expires_in', $data['expires_in']);
        $user->put('access_token', $data['access_token']);
        // $user->put('refresh_token', $data['refresh_token']);


        return Response::json($user);
    }
}
