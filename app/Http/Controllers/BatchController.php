<?php

namespace App\Http\Controllers;

use App\Batch;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class BatchController extends Controller
{
    const VIEW_PATH = 'authority.batches.';
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $batches = Batch::latest()->get();

        return view(self::VIEW_PATH. 'index', compact('batches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $batch = new Batch();

        return view(self::VIEW_PATH .'edit', compact('batch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $this->validator($input)->validate();

        try {
            $batch = Batch::create($input);
        } catch (\Throwable $exception) {
            Log::debug($exception->getMessage());
            return back()
                ->with(["error" => "Something Wrong. Please Try Again"])
                ->withInput();
        }

        return back()->with(["success" => "Batch Successfully Created"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Batch  $batch
     * @return Factory|View
     */
    public function show(Batch $batch)
    {
        return view(self::VIEW_PATH .'view', compact('batch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Batch  $batch
     * @return Factory|View
     */
    public function edit(Batch $batch)
    {
        return view(self::VIEW_PATH .'edit', compact( 'batch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Batch  $batch
     * @return RedirectResponse
     */
    public function update(Request $request, Batch $batch)
    {
        $input = $request->except('_token');

        $this->validator($input)->validate();

        try {
            $batch->update($input);
        } catch (\Throwable $exception) {
            Log::debug($exception->getMessage());
            return back()
                ->with(["error" => "Something Wrong. Please Try Again"])
                ->withInput();
        }

        return back()->with(["success" => "Batch Successfully Created"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Batch  $batch
     * @return RedirectResponse
     */
    public function destroy(Batch $batch)
    {
        try {
            $batch->delete();
        } catch (\Throwable $exception) {
            return redirect()->route('admin.batches.index')
                ->with(["error" => "Something Wrong. Please Try Again"]);
        }
        return redirect()->route('admin.batches.index')
            ->with(["success" => "Student Successfully Updated"]);
    }

    public function validator(array $data, $id=null)
    {
        $rule = [
            'batch_name' => ['required'],
            'session' => ['required'],
            'course_title' => ['required'],
        ];
        return Validator::make($data, $rule, [
            'batch_name.required' => 'Batch Required'
        ]);
    }
}
