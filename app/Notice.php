<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $fillable = ['batch_id', 'type', 'priority', 'title', 'content', 'attachment', 'status'];

    const TYPE_CLASS = 'class';
    const TYPE_EXAM = 'exam';
    const TYPE_OTHERS = 'others';

    public function scopeCurrentBatch()
    {

    }

    public function batch()
    {
        return $this->belongsTo(Batch::class);
    }
}
