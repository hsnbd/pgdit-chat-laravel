<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $fillable = ['batch_name', 'session', 'course_title', 'status'];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function students()
    {
        return $this->users()->students();
    }

    public function notices()
    {
        return $this->hasMany(Notice::class);
    }
}
