<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Collection;

class MessageRepository extends BaseRepository {

    /**
     * @inheritDoc
     */
    protected function setModel(): string
    {
        return User::class;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->students()->get();
    }
}
