<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BatchRepository extends BaseRepository {

    /**
     * @inheritDoc
     */
    protected function setModel(): string
    {
        return User::class;
    }
}
