<?php

namespace App\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements RepositoryInterface {

    /**
     * Model instance
     * @var
     */
    protected $model;

    public function __construct()
    {
        $model = $this->setModel();
        $this->model = app($model);
    }

    /**
     * @return string
     */
    abstract protected function setModel(): string;

    public function __call($method, $parameters) {
        return $this->model->$method(...$parameters);
    }
    public function getModel(): Model
    {
        return $this->model;
    }
}
