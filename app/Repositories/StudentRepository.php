<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Collection;

class StudentRepository extends BaseRepository {

    /**
     * @inheritDoc
     */
    protected function setModel(): string
    {
        return User::class;
    }
}
