<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const ROLE_STUDENT = 'student';
    const ROLE_CR = 'cr';
    const ROLE_TEACHER = 'teacher';
    const ROLE_STAFF = 'staff';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image', 'batch_id', 'gender', 'dob', 'roll', 'role', 'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at', 'dob'
    ];


    public function hasRole(...$role)
    {
        return in_array(Auth::user()->role, $role);
    }

    /**
     * Select Only Student
     */
    public function scopeStudents($query)
    {
        return $query->whereIn('role', [self::ROLE_STUDENT, self::ROLE_CR]);
    }

    public function batch()
    {
        return $this->belongsTo(Batch::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
