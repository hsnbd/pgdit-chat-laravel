<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->enum('gender', ['Male', 'Female']);
            $table->date('dob')->nullable();
            $table->unsignedInteger('roll')->nullable();
            $table->unsignedInteger('batch_id');
            $table->enum('role', ['student', 'teacher', 'staff', 'cr'])->default('student');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('image', 255)->nullable();
//            $table->unsignedTinyInteger('student_id')->nullable();
            $table->rememberToken();
            $table->softDeletes();
//            $table->foreign('batch_id')->references('id')->on('batches')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
