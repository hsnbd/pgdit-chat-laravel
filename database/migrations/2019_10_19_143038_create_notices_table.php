<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('batch_id');
            $table->enum('type', ['class', 'exam', 'others'])->default('class');
            $table->unsignedTinyInteger('priority')->default(1);
            $table->string('title', 191);
            $table->string('content', 2000);
            $table->string('attachment')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
//            $table->foreign('batch_id')->references('id')->on('batches')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notices');
    }
}
