<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:api')->get(
    '/user',
    function (Request $request) {
        $user = $request->user();

        if ($user->batch->status != 'active') {
            return response()->json(["errors" => "Access Forbidden"], 403);
        }

        if ($user->role === User::ROLE_TEACHER || $user->role === User::ROLE_STAFF) {
            return response()->json(["errors" => "Only Allowed For Student."], 403);
        }
        return $user;
    }
);

Route::post('register', 'API\Auth\RegisterController@register');
Route::post('login', 'API\Auth\LoginController@login');
Route::post('refresh', 'API\Auth\LoginController@refresh');

Route::post('password/email', 'API\Auth\ForgotPasswordController@getResetToken');
Route::post('password/reset', 'API\Auth\ResetPasswordController@reset');

//this route for custom oauth token response;
Route::post('oauth/token', 'API\Auth\AccessTokenController@issueToken');


Route::middleware('auth:api')->get(
    '/notices',
    function (Request $request) {
        return \App\Notice::where('batch_id', $request->user()->batch_id)->get();
    }
);

Route::middleware('auth:api')->post(
    '/messages',
    function (Request $request) {
        return \App\Message::where('batch_id', $request->user()->batch_id)
            ->with('user')->get();
    }
);

Route::middleware('auth:api')->post(
    '/message',
    function (Request $request) {
        $user = $request->user();
        $message = \App\Message::create(
            [
                'batch_id' => $user->batch_id,
                'sender_id' => $user->id,
                'content' => $request->message
            ]
        );

        event(new \App\Events\pushMessage($message));

        return $message;
    }
);
Route::middleware('auth:api')->post('/profile/update', 'API\Auth\UserController@update');
