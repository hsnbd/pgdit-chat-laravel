<?php
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/register', function (){
   return redirect('/login');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'auth'], function(){
    Route::resource('students', 'StudentController');
    Route::resource('authorities', 'AuthorityController');
    Route::resource('notices', 'NoticeController');
    Route::resource('batches', 'BatchController');
});
