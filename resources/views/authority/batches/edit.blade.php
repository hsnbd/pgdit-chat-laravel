@extends('layouts.back')

@php
    $edit = isset($batch->id) && !empty($batch->id);
@endphp

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <!-- /.col-->
            <div class="col-sm-12">
                @include('notifications.message')
                <div class="card card-accent-info">
                    <div class="card-header">
                        <h5>
                            <strong>Batch</strong>
                            <small>{{$edit ? 'Edit' : 'Add'}}</small>
                        </h5>
                    </div>
                    <form method="post"
                          action="{{$edit ? route('admin.batches.update', $batch->id) : route('admin.batches.store')}}"
                          enctype="multipart/form-data">
                        @csrf
                        @if($edit)
                            @method('put')
                        @endif

                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="batch_name">Batch No</label>
                                    <input type="text" name="batch_name" value="{{$edit ? $batch->batch_name : ''}}"
                                           class="form-control @error('batch_name') is-invalid @enderror">
                                    @error('batch_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="session">Session</label>
                                    <input type="text" name="session" value="{{$edit ? $batch->session : ''}}"
                                           class="form-control @error('session') is-invalid @enderror">
                                    @error('session')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="session">Course Title</label>
                                    <input type="text" name="course_title" value="{{$edit ? $batch->course_title : ''}}"
                                           class="form-control @error('course_title') is-invalid @enderror">
                                    @error('course_title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                @if($edit)
                                    <div class="form-group col-md-6">
                                        <label for="status">Status</label>
                                        <select type="text" name="status"
                                                class="form-control @error('status') is-invalid @enderror">
                                            <option value="active" {{$batch->status === 'active' ? 'selected': ''}}>Active</option>
                                            <option value="inactive" {{$batch->status === 'inactive' ? 'selected': ''}}>Inactive</option>
                                        </select>
                                        @error('status')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="card-footer">
                            <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">
                                <i class="fa fa-dot-circle-o"></i> Cancel</a>
                            <button class="btn btn-sm btn-primary" type="submit">
                                <i class="fa fa-dot-circle-o"></i> {{$edit ? 'Save Change' : 'Add Batch' }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        var loadFile = function (event) {
            $('.pdf-name').text(event.target.files[0].name);
        };
    </script>
@endpush
