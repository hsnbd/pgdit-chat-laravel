@extends('layouts.back')

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                @include('notifications.message')
                <div class="card card-accent-dark">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>Authority lists

                        <div class="card-header-actions btn-group btn-group-sm">
                            <a href="{{route('admin.authorities.create')}}"
                               class="card-header-action btn btn-outline-dark">
                                Add New
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="datatable table table-striped">
                            <thead>
                            <tr>
                                <th>Batch</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($authorities as $authority)
                                <tr>
                                    <td>{{optional($authority->batch)->batch_name }}</td>
                                    <td>{{$authority->name}}</td>
                                    <td>{{\Illuminate\Support\Str::ucfirst($authority->role)}}</td>
                                    <td>{{$authority->email}}</td>
                                    <td>{{$authority->gender}}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group">
                                            <a
                                                data-toggle="tooltip"
                                                title="View This Authority"
                                                href="{{route('admin.authorities.show', $authority->id)}}"
                                                class="btn btn-outline-info">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            <a
                                                data-toggle="tooltip"
                                                title="Edit This Authority"
                                                href="{{route('admin.authorities.edit', $authority->id)}}"
                                                class="btn btn-outline-warning">
                                                <i class="fa fa-edit" aria-hidden="true"></i>
                                            </a>
                                            <button
                                                data-toggle="tooltip"
                                                title="Delete This Authority"
                                                onclick="ConfirmDelete('deleteForm_'+'{{$authority->id}}');" type="button"
                                                class="card-header-action btn btn-outline-danger">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>
                                            <form id="deleteForm_{{$authority->id}}"
                                                  action="{{route('admin.authorities.destroy', $authority->id)}}"
                                                  method="post">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">Empty table</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>


    <!-- Modal -->
    <div class="modal" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2>Are You Sure?</h2>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button onclick="document.getElementById(elm_to_delete).submit();" type="button"
                            class="btn btn-danger">Confirm
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var elm_to_delete;

        function ConfirmDelete(elm = null) {
            elm_to_delete = elm;
            $("#confirm-delete").modal('show');
        }

        $(function () {
            $('.datatable').DataTable();
        });
    </script>
@endpush
