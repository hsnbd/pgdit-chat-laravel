@extends('layouts.back')

@php
    $edit = isset($student->id) && !empty($student->id);
@endphp

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <!-- /.col-->
            <div class="col-sm-12">
                @include('notifications.message')
                <div class="card card-accent-info">
                    <div class="card-header">
                        <h5>
                            <strong>Student</strong>
                            <small>{{$edit ? 'Edit' : 'Add'}}</small>
                        </h5>
                    </div>
                    <form method="post"
                          action="{{$edit ? route('admin.students.update', $student->id) : route('admin.students.store')}}"
                          enctype="multipart/form-data">
                        @csrf
                        @if($edit)
                            @method('put')
                        @endif

                        <div class="card-body">
                            <div class="row">
                                @can('viewAny', \App\User::class)
                                    <div class="form-group col-md-6">
                                        <label for="batch_id">Select Batch</label>
                                        <select class="form-control
                                            @error('batch_id') is-invalid @enderror"
                                                id="batch_id" name="batch_id">
                                            @foreach($batches as $batch)
                                                <option value="{{$batch->id}}"
                                                        {{old('batch_id', $edit ? $student->batch_id : 0) == $batch->id ? 'selected' : ''}}>
                                                    {{$batch->batch_name . '('. $batch->course_title . ')'}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('batch_id')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                @else
                                    <input type="hidden" value="{{auth()->user()->batch_id}}" name="batch_id" />
                                @endcan

                                @can('viewAny', \App\User::class)
                                    <div class="form-group col-md-6">
                                        <label for="role">Select Role</label>
                                        <select class="form-control
                                            @error('role') is-invalid @enderror"
                                                id="role" name="role">
                                            <option selected value="">Select Role</option>
                                            @foreach($roles as $key => $role)
                                                <option value="{{$key}}"
                                                        {{old('role', $edit ? $student->role : \App\User::ROLE_STUDENT) == $key ? 'selected' : ''}}>
                                                    {{$role}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('role')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                @endcan

                                <div class="form-group col-md-6">
                                    <label for="name">Student Name</label>
                                    <input class="form-control  @error('name') is-invalid @enderror" type="text"
                                           name="name" value="{{old('name', $student->name)}}"/>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="roll">Roll</label>
                                    <input class="form-control  @error('roll') is-invalid @enderror" type="number"
                                           name="roll" value="{{old('roll', $student->roll)}}"/>
                                    @error('roll')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input class="form-control  @error('email') is-invalid @enderror" type="email"
                                           name="email" value="{{old('email', $student->email)}}"/>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>


                                @if(!$edit)
                                    <div class="form-group col-md-6">
                                        <label for="password">Password (Default: 123456)</label>
                                        <input class="form-control  @error('password') is-invalid @enderror"
                                               type="password"
                                               name="password" value="{{'123456'}}"/>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                @endif

                                <div class="form-group col-md-6">
                                    <label for="gender">Gender</label>
                                    <select class="form-control  @error('gender') is-invalid @enderror" name="gender">
                                        <option selected disabled>Select Gender</option>
                                        <option
                                                value="Male"
                                                {{old('gender', $student->gender) === 'Male' ? 'selected' : ''}}>Male
                                        </option>
                                        <option
                                                value="Female"
                                                {{old('gender', $student->gender) === 'Female' ? 'selected' : ''}}>
                                            Female
                                        </option>
                                    </select>
                                    @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="dob">Date Of Birth</label>
                                    <input type="text" name="dob"
                                           value="{{$edit ? optional($student->dob)->format('Y-m-d') : (new \DateTime())->modify('-18 years')->format('Y-m-d')}}"
                                           class="form-control @error('dob') is-invalid @enderror"
                                           id="dob">
                                    @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    @if($student->image)
                                        <img id="image_output" style="width: 100px"
                                             src="{{asset('storage/students/'.$student->image)}}"/>
                                        <button type="button" class="remove-image btn btn-sm btn-danger">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    @else
                                        <img id="image_output" style="width: 80px" src="{{url('/')}}/assets/blank.png"/>
                                    @endif
                                    <div class="clearfix"></div>
                                    <label for="image">Student Image</label>
                                    <div class="custom-file">
                                        <input onchange="loadFile(event)" type="file" accept="image/*" name="image"
                                               class="custom-file-input  @error('image') is-invalid @enderror"
                                               id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose Image</label>
                                    </div>
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <input type="hidden" name="image_status" id="image_status" value="1"/>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">
                                <i class="fa fa-dot-circle-o"></i> Cancel</a>
                            <button class="btn btn-sm btn-primary" type="submit">
                                <i class="fa fa-dot-circle-o"></i> {{$edit ? 'Save Change' : 'Add Student' }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        var loadFile = function (event) {
            $("#image_output").attr('src', URL.createObjectURL(event.target.files[0]));
            if ($("#image_output").is(':hidden')) {
                $("#image_output").show(300);
                $(".remove-image").show(300);
            }
            $('#image_status').val(1);
        };

        $(function () {
            $('.remove-image').on('click', function () {
                $('#image_output').hide(300);
                $(this).hide(300);
                $('#image_status').val(0);
            });

            $('#dob').flatpickr({
                enableTime: false,
                dateFormat: "Y-m-d",
                enable: [
                    {
                        from: "1990-01-01",
                        // to: new Date()
                        to: "{{(new \DateTime())->modify('-18 years')->format('Y-m-d')}}"
                    }
                ]
            });
        })

    </script>
@endpush
