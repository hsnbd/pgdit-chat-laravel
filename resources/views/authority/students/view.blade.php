@extends('layouts.back')

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                @include('notifications.message')
                <div class="card card-accent-primary">
                    <div class="card-header">
                        {{$student->name}}
                        <div class="card-header-actions btn-group btn-group-sm">
                            <a href="{{route('admin.students.edit', $student->id)}}" class="card-header-action btn btn-outline-warning">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>
                            <button data-toggle="modal" data-target="#confirm-delete"  type="button" class="card-header-action btn btn-outline-danger">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                            <form id="deleteForm" action="{{route('admin.students.destroy', $student->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                @if($student->image)
                                    <img style="width: 100%" src="{{asset('storage/students/' .$student->image)}}"/>
                                @else
                                    <img id="logo_output" style="width: 80px" src="{{url('/')}}/assets/blank.png"/>
                                @endif
                            </div>
                            <div class="col-md-9">
                                <h3 class="card-title">Student Profile</h3>

                                <table class="table">
                                    <tr>
                                        <th>Student Name:</th>
                                        <td>{{$student->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Roll</th>
                                        <td>{{$student->roll}}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{{$student->email}}</td>
                                    </tr>
                                    <tr>
                                        <th>Gender</th>
                                        <td>{{$student->gender}}</td>
                                    </tr>
                                    <tr>
                                        <th>Date Of Birth</th>
                                        <td>{{optional($student->dob)->format('d/m/Y') }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2>Are You Sure?</h2>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button onclick="document.getElementById('deleteForm').submit();" type="button" class="btn btn-danger">Confirm</button>
                </div>
            </div>
        </div>
    </div>
@endsection
