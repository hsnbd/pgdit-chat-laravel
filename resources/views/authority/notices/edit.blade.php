@extends('layouts.back')

@php
    $edit = isset($notice->id) && !empty($notice->id);
@endphp
{{--                    'batch_id', 'type', 'priority', 'title', 'content', 'attachment', 'status'--}}

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <!-- /.col-->
            <div class="col-sm-12">
                @include('notifications.message')
                <div class="card card-accent-info">
                    <div class="card-header">
                        <h5>
                            <strong>Notice</strong>
                            <small>{{$edit ? 'Edit' : 'Add'}}</small>
                        </h5>
                    </div>
                    <form method="post"
                          action="{{$edit ? route('admin.notices.update', $notice->id) : route('admin.notices.store')}}"
                          enctype="multipart/form-data">
                        @csrf
                        @if($edit)
                            @method('put')
                        @endif

                        <div class="card-body">
                            <div class="row">
                                @can('viewAny', \App\User::class)
                                    <div class="form-group col-md-6">
                                        <label for="batch_id">Select Batch</label>
                                        <select class="form-control
                                            @error('batch_id') is-invalid @enderror"
                                                id="batch_id" name="batch_id">
                                            @foreach($batches as $batch)
                                                <option value="{{$batch->id}}"
                                                        {{old('batch_id', $edit ? $notice->batch_id : 0) == $batch->id ? 'selected' : ''}}>
                                                    {{$batch->batch_name . '('. $batch->course_title . ')'}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('batch_id')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                @else
                                    <input type="hidden" value="{{auth()->user()->batch_id}}" name="batch_id" />
                                @endcan

                                <div class="form-group col-md-6">
                                    <label for="type">Notice About</label>
                                    <select class="form-control  @error('type') is-invalid @enderror" name="type">
                                        <option selected disabled>Select Notice Type</option>
                                        <option value="class"{{old('type', $notice->type) === 'class' ? 'selected' : ''}}>Class</option>
                                        <option value="exam"{{old('type', $notice->type) === 'exam' ? 'selected' : ''}}>Exam</option>
                                        <option value="others"{{old('type', $notice->type) === 'others' ? 'selected' : ''}}>Others</option>
                                    </select>
                                    @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="content">Details</label>
                                    <textarea class="form-control" id="content" name="content">{{$edit ? $notice->content : ''}}</textarea>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="title">Title</label>
                                    <input class="form-control  @error('title') is-invalid @enderror" type="text"
                                           name="title" value="{{old('title', $notice->title)}}"/>
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="clearfix"></div>
                                    <label for="attachment">Attachment PDF (<span class="pdf-name">{{$edit ? $notice->attachment : 'Empty'}}</span>)</label>
                                    <div class="custom-file">
                                        <input onchange="loadFile(event)" type="file" name="attachment" accept="application/pdf"
                                               class="custom-file-input  @error('attachment') is-invalid @enderror"
                                               id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose PDFs</label>
                                    </div>
                                    @error('attachment')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <a class="btn btn-sm btn-danger" href="{{ url()->previous() }}">
                                <i class="fa fa-dot-circle-o"></i> Cancel</a>
                            <button class="btn btn-sm btn-primary" type="submit">
                                <i class="fa fa-dot-circle-o"></i> {{$edit ? 'Save Change' : 'Add Notice' }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        var loadFile = function (event) {
            $('.pdf-name').text(event.target.files[0].name);
        };
    </script>
@endpush
