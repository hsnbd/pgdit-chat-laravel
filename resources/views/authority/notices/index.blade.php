@extends('layouts.back')

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                @include('notifications.message')
                <div class="card card-accent-dark">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Notices

                        <div class="card-header-actions btn-group btn-group-sm">
                            <a href="{{route('admin.notices.create')}}"
                               class="card-header-action btn btn-outline-dark">
                                Add New
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="datatable table table-striped">
                            <thead>
                            <tr>
                                <th>Type</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>attachment</th>
                                <th>status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($notices as $notice)
                                <tr>
                                    <td>{{$notice->type}}</td>
                                    <td>{{$notice->title}}</td>
                                    <td>{{$notice->content}}</td>
                                    <td>
                                        <a target="_blank" href="{{asset('storage/notices/'.$notice->attachment)}}">
                                            <i class="fa fa-download"></i>
                                        </a>
                                    </td>
                                    <td>{{$notice->status}}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group">
                                            <a
                                                data-toggle="tooltip"
                                                title="View This Company"
                                                href="{{route('admin.notices.show', $notice->id)}}"
                                                class="btn btn-outline-info">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            <a
                                                data-toggle="tooltip"
                                                title="Edit This Company"
                                                href="{{route('admin.notices.edit', $notice->id)}}"
                                                class="btn btn-outline-warning">
                                                <i class="fa fa-edit" aria-hidden="true"></i>
                                            </a>
                                            <button
                                                data-toggle="tooltip"
                                                title="Delete This Company"
                                                onclick="ConfirmDelete('deleteForm_'+'{{$notice->id}}');" type="button"
                                                class="card-header-action btn btn-outline-danger">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>
                                            <form id="deleteForm_{{$notice->id}}"
                                                  action="{{route('admin.notices.destroy', $notice->id)}}"
                                                  method="post">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">Empty table</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>


    <!-- Modal -->
    <div class="modal" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Please Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2>Are You Sure?</h2>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button onclick="document.getElementById(elm_to_delete).submit();" type="button"
                            class="btn btn-danger">Confirm
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var elm_to_delete;

        function ConfirmDelete(elm = null) {
            elm_to_delete = elm;
            $("#confirm-delete").modal('show');
        }

        $(function () {
            $('.datatable').DataTable();
        });
    </script>
@endpush
