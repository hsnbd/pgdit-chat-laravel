@extends('layouts.back')

@section('title', 'Profile')

@section('content')
 <div class="animated fadeIn">
     @include('notifications.message')
    <div class="row">
        <!-- /.col-->
        <div class="col-sm-6">
            <div class="card card-accent-info">
                <div class="card-header">
                    <strong>User</strong>
                    <small>Profile</small>
                </div>
                <form action="{{route('profile.update')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="first_name">First name</label>
                            <input name="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" value="{{auth()->user()->first_name}}">
                            @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group ">
                            <label for="last_name">Last name</label>
                            <input name="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" value="{{auth()->user()->last_name}}">
                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">
                            <i class="fa fa-dot-circle-o"></i> Save Change</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.col-->
    </div>
     <div class="row">
         <!-- /.col-->
         <div class="col-sm-6">
             <div class="card card-accent-danger">
                 <div class="card-header">
                     <strong>User</strong>
                     <small>Security</small>
                 </div>
                 <div class="card-body">
                     <div class="row">
                         <div class="col-md-4">
                             <a href="{{route('profile.reset.form')}}" class="btn btn-ghost-danger">
                                 <i class="fa fa-lock"></i> Reset Password</a>
                         </div>
                         <div class="col-md-4">
                             <a href="{{route('profile.reset.2fa.form')}}" class="btn btn-ghost-danger">
                                 <i class="fa fa-user-secret"></i> Reset 2FA Token</a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <!-- /.col-->
     </div>
</div>
@endsection