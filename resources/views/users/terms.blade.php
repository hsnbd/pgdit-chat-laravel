@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Terms & Policy
                    </div>
                    <form id="terms_confirm" action="{{route('terms.confirm')}}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                @for($i=1; $i <= count($terms); $i++)
                                    <div class="col-md-12 mb-5">
                                        <div class="card border-0">
                                            <a class="text-white" data-toggle="collapse" href="#collapse_{{$i}}" aria-expanded="true" aria-controls="collapse_{{$i}}">
                                                <div class="card-header bg-dark" onclick="$(this).find('i').toggleClass('fa-plus fa-minus')">
                                                    {{\App\Terms::$terms_types[$i]}}
                                                    <span class="float-right text-white">
                                                        <i class="fa fa-plus"></i>
                                                    </span>
                                                </div>
                                            </a>

                                            <div id="collapse_{{$i}}" class="card-body collapse" role="tabpanel" aria-labelledby="headingOne">
                                                @foreach($terms[$i] as $trm)
                                                    {!! $trm->details !!}
                                                @endforeach
                                            </div>
                                            <div class="card-footer">
                                                <input type="hidden" name="{{\App\User::$terms[$i]}}" value="{{auth()->user()->{\App\User::$terms[$i]} }}">
                                                @if(auth()->user()->{\App\User::$terms[$i]} == 1)
                                                    <button type="button" class="btn btn-success">Accepted</button>
                                                @else
                                                    <button onclick="accept('{{\App\User::$terms[$i]}}')" type="button" class="btn btn-primary">Accept</button>
                                                    <button onclick="document.getElementById('logout').submit()" type="button" class="btn btn-danger">Decline</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <form id="logout" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
@endsection

@push('scripts')
    <script type="text/javascript">
        function accept(index) {
            $("input[name="+index+"]").val(1);
            document.getElementById('terms_confirm').submit();
        }
    </script>
@endpush