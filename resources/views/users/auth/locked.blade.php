@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-danger">
                        Your Account is locked
                    </div>
                    <div class="card-body">
                        <h2>Please Contact your company to unlock</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection