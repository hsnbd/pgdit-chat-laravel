@extends('layouts.back')

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                @include('notifications.message')
            </div>
            <!-- /.col-->
            <div class="col-sm-6">
                <div class="card card-accent-danger">
                    <form method="post" action="{{route('profile.reset.2fa')}}">
                        @csrf
                        <input type="hidden" name="2fa" value="{{$google2fa_secret}}" />
                        <div class="card-header">
                            <strong>2FA</strong>
                            <small>Reset</small>

                        </div>
                        <div class="card-body">
                            <span class="text-warning font-weight-bold">Please Scan QR Code and reset 2FA</span>
                            <div class="form-group">
                                <img src="{{$QR_Image}}" class="">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-danger" type="submit">
                                <i class="fa fa-dot-circle-o"></i> Reset 2FA</button>

                            <a href="{{route('profile')}}" class="btn btn-sm btn-info">
                                <i class="fa fa-dot-circle-o"></i> Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col-->
        </div>
@endsection