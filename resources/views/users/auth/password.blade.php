@extends('layouts.back')

@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                @include('notifications.message')
            </div>
            <!-- /.col-->
            <div class="col-sm-6">
                <div class="card card-accent-danger">
                    <form method="post" action="{{route('profile.reset')}}">
                        @csrf
                        <div class="card-header">
                            <strong>Password</strong>
                            <small>Reset</small>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="">Old Password</label>
                                <input id="old_password" type="password" placeholder="Old Password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" required>
                                @error('old_password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">New Password</label>
                                <input id="password" type="password" placeholder="New Password" class="form-control @error('password') is-invalid @enderror" name="password" required>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Confirm New Password</label>
                                <input id="password_confirmation" type="password" placeholder="Confirm New Password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required>
                                @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-danger" type="submit">
                                <i class="fa fa-dot-circle-o"></i> Reset Password</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.col-->
        </div>
@endsection