@extends('layouts.back')

@push('styles')
    <style>
        iframe {
            min-height: 500px;
        }
        .data-header {
            padding: 10px;
            font-weight: bold;
            background: #777;
            color: #fff;
        }
    </style>
@endpush

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-success text-center" style="padding: 80px 0;">
                <h3>You are a valid user.</h3>
                <p>
                    You have no permissions at this time to view data.
                </p>
                <p>
                    Please contact your company administrator.
                </p>
            </div>
        </div>
    </div>
@endsection