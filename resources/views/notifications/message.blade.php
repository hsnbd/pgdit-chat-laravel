@if(session('success') || session('error'))
    <div class="border-{{session('success') ? 'success' : 'danger'}} alert alert-{{session('success') ? 'success' : 'danger'}} alert-dismissible fade show" role="alert">
        {{session('success') ?? session('error')}}
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif