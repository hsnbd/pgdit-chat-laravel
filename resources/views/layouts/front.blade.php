<!DOCTYPE html>
<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>PGDIT Community Platform</title>
    <!-- Icons-->
    <link href="{{url('/')}}/coreui/vendors/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{url('/')}}/coreui/css/style.css" rel="stylesheet">
    <link href="{{url('/')}}/css/custom.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
</head>
<body class="app flex-row align-items-center">
<div class="container">
    @yield('content')
</div>
<!-- CoreUI and necessary plugins-->
<script src="{{url('/')}}/coreui/vendors/jquery/js/jquery.min.js"></script>
<script src="{{url('/')}}/coreui/vendors/popper.js/js/popper.min.js"></script>
</body>
</html>
