<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="VigilWatch - A better solution for you">
    <meta name="author" content="Shane">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keyword" content="watch, vigil">
    <title>PGDIT | Student Communication Platform</title>
    <!-- Icons-->
    <link href="{{url('/')}}/coreui/vendors/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Main styles for this application with coreui-->
    <link href="{{url('/')}}/coreui/css/style.css" rel="stylesheet">
    <link href="{{url('/')}}/coreui/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
    <link href="{{url('/')}}/css/flatpickr.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/css/dataTables.bootstrap4.min.css">

@stack('styles')
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar bg-gray-dark">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="text-light navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <img src="{{url('/')}}/assets/iit-logo.png" style="width: 95%;" title="Logo">
    </a>
    <button class="navbar-toggler text-light sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="menu-icon"><i class="fa fa-bars"></i></span>
    </button>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link text-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name }} &nbsp;
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="#">
                    <i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item" href="#" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i> Logout</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</header>
<div id="app" class="app-body">

    <!--
    | sidebar start
    -->
    <!-- check if user already verified email -->
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav" style="font-size: 0.8rem;">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('home')}}" >
                        <i class="nav-icon icon-speedometer"></i>Dashboard
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route("admin.students.index")}}">
                        <i class="nav-icon icon-list"></i>Student Management</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route("admin.notices.index")}}">
                        <i class="nav-icon icon-list"></i>Notice Management</a>
                </li>

                @can('viewAny', \App\User::class)
                    <li class="nav-item">
                        <a class="nav-link" href="{{route("admin.authorities.index")}}">
                            <i class="nav-icon icon-list"></i>Authority Management</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route("admin.batches.index")}}">
                            <i class="nav-icon icon-compass"></i>Batch Management</a>
                    </li>
                @endcan
            </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>

    <!--sidebar end -->

    <main class="main mt-4">
        <div class="container-fluid">
            <!-- entry point -->
            @yield('content')
        </div>
    </main>
</div>

<footer class="app-footer">
    <div>
        <span>&copy; {{date('Y')}} IIT, University of Dhaka.</span>
    </div>
</footer>
<!-- CoreUI and necessary plugins-->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{url('/')}}/coreui/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
<script src="{{url('/')}}/coreui/vendors/@coreui/coreui/js/coreui.min.js"></script>
<script src="{{url('/')}}/js/flatpickr.js"></script>
<script src="{{url('/')}}/js/jquery.dataTables.min.js"></script>
<script src="{{url('/')}}/js/dataTables.bootstrap4.min.js"></script>

<!-- this is the pointer for script from other view file that extended this master view -->
<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@stack('scripts')
</body>
</html>
