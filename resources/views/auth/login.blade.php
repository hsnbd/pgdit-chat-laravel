@extends('layouts.front')

@section('content')
    {{--@dd($errors)--}}
    <div class="row justify-content-center">
        <div class="col-md-4 login">
            <div class="card-group">
                <div class="card p-4">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="card-body">
                            <div class="col-12 mb-4">
                                <img src="{{url('/assets/iit-logo.png')}}" style="width: 100%" />
                            </div>
                            <h6 class="text-muted mb-4 font-weight-bold text-center">Sign In to your account</h6>
                            <div class="input-group mb-3">
                                <input id="email" type="email" placeholder="Email/Username" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group mb-4">
                                <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn login-btn btn-success px-4" type="submit">Login</button>
                                </div>
                                <div class="col-12 text-center mt-2">
                                    <a href="{{ route('password.request') }}">Forgot password?</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
