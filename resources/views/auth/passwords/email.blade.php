@extends('layouts.front')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8 login">
            <div class="card-group">
                <div class="card bg-light p-4">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="card-body">
                            <h2>Password Reset</h2>
                            <p class="text-muted">
                                Enter your email, username and 2fa to reset password
                            </p>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-envelope"></i>
                                    </span>
                                </div>
                                <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-lock"></i>
                                    </span>
                                </div>
                                <input id="_2fa" type="text" placeholder="2FA Code" class="form-control @error('_2fa') is-invalid @enderror" name="_2fa" value="" required >
                                @error('_2fa')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn btn-warning px-4" type="submit">Send Password Reset Request</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card text-white bg-dark py-5 d-md-down-none" style="width:44%">
                    <div class="card-body text-center">
                        <div>
                            <h2>Log In</h2>
                            <p>Log in to your account with your valid credentials</p>
                            <a href="{{route('login')}}" class="btn btn-success active mt-3" type="button">Log In</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection