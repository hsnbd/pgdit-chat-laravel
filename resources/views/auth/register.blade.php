@extends('layouts.front')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card bg-light mx-4">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="card-body p-4">
                        <h1>Register</h1>
                        <p class="text-muted">Create your account</p>
                        <div class="form-group row">
                            <div class="col-md-12 input-group mb-3">
                                <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="icon-user"></i>
                          </span>
                                </div>
                                <input id="name" type="text" placeholder="Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">@</span>
                                </div>
                                <input id="email" type="email" placeholder="Email Address" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 input-group mb-3">
                                <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="icon-lock"></i>
                          </span>
                                </div>
                                <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autofocus>
                                @error('password')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                            <div class="col-md-12 input-group mb-4">
                                <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="icon-lock"></i>
                          </span>
                                </div>
                                <input id="password_confirmation" type="password" placeholder="Confirm Password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" value="{{ old('password_confirmation') }}" required autofocus>
                                @error('password_confirmation')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <button class="btn btn-block btn-success" type="submit">Create Account</button>
                    </div>
                </form>
                <div class="card-footer p-4">
                    <div class="row">
                        <div class="col-12 text-center">
                            <a href="{{route('login')}}" >Already have an account?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
