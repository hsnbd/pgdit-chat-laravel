@extends('layouts.back')
@push('styles')
    <style type="text/css">
        .bg-gradient-primary {
            background: #1f1498;
            background: linear-gradient(45deg, #321fdb 0%, #1f1498 100%);
            border-color: #1f1498;
        }
        .bg-gradient-info {
            background: #2982cc;
            background: linear-gradient(45deg, #39f 0%, #2982cc 100%);
            border-color: #2982cc;
        }
        .bg-gradient-warning {
            background: #f6960b;
            background: linear-gradient(45deg, #f9b115 0%, #f6960b 100%);
            border-color: #f6960b;
        }
        .bg-gradient-danger {
            background: #d93737;
            background: linear-gradient(45deg, #e55353 0%, #d93737 100%);
            border-color: #d93737;
        }
    </style>
@endpush
@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                @include('notifications.message')
                <div class="row">
                    <div class="col-md-3">
                        <div class="card text-white bg-gradient-primary">
                            <div class="card-body justify-content-center align-content-center">
                                <h4>Total Students {{$totalStudent}}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card text-white bg-gradient-info">
                            <div class="card-body justify-content-center align-content-center">
                                <h4>Active Students {{$activeStudent}}</h4>
                            </div>
                        </div>
                    </div>
                    @can('viewAny', \App\User::class)
                        <div class="col-md-3">
                            <div class="card text-white bg-gradient-warning">
                                <div class="card-body justify-content-center align-content-center">
                                    <h4>Total Batches {{$totalBatch}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card text-white bg-gradient-danger">
                                <div class="card-body justify-content-center align-content-center">
                                    <h4>Active Batches {{$activeBatch}}</h4>
                                </div>
                            </div>
                        </div>
                    @endcan
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>
@endsection

@push('scripts')
@endpush
